Pod::Spec.new do |s|
  s.name                        = "InsideMobileiOS"
  s.version                     = "1.0.5"
  s.summary                     = "Powerfront INSIDE Mobile SDK for iOS apps"
  s.description                 = <<-DESC
  The Inside Mobile iOS provides the necessary functions to track user and building INSIDE chat into an iOS Mobile App.
  DESC
  s.homepage                    = "https://www.powerfront.com"
  s.documentation_url           = "https://insidemobile.bitbucket.io/ios/"
  s.license                     = { :type => 'MIT', :text => <<-LICENSE
    © 2019 - Powerfront Inc.
    LICENSE
  }
  s.author                      = { "Greg Platt" => "greg.platt@powerfront.com" }
  s.platform                    = :ios, "10.0"
  s.source                      = { :git => "https://bitbucket.org/insideteam/insidemobileios.git", :tag => "#{s.version}" }
  s.requires_arc                = true
  s.ios.vendored_frameworks     = 'InsideMobileiOS/InsideMobileiOS.framework'
  s.source_files                = 'InsideMobileiOS/InsideMobileiOS.framework/Headers/*.h'
  s.public_header_files         = "InsideMobileiOS/InsideMobileiOS.framework/Headers/*.h"
end
